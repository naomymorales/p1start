package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
//import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CarList;
//import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() { //method to get all the cars in the list in the server
		SortedList<Car> listacarros = CarList.getInstance();
		Car[] array = new Car[listacarros.size()];
		int i=0;
		for(Car car: listacarros) {
			array[i]=car;
			i++;
		}
		return array;
			
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON) //method to get a car by using it's id
	public Car getCar(@PathParam("id")long id) throws NotFoundException  {
		SortedList<Car> listacarros = CarList.getInstance();
		for(Car car: listacarros) {
			if(car.getCarId()==id) {
				return car;
			}
		}
		throw new WebApplicationException(404); //when car is not found
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON) //adding a car to the list in the server
	public Response addCar(Car newCar) {
		SortedList<Car> listacarros = CarList.getInstance();
		for(int i=0; i<listacarros.size();i++) {
			if(listacarros.get(i).getCarId()==newCar.getCarId()) {
				return Response.status(Response.Status.CONFLICT).build();
				
			}
		}
		listacarros.add(newCar);
		return Response.status(201).build();
	}
	
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON) //Updating an existing car in the list in the server
	public Response updateCar(Car newcar)	{
		SortedList<Car> listacarros = CarList.getInstance();
		for(Car car: listacarros) {
			if(car.getCarId()==newcar.getCarId()) {
				listacarros.remove(car);
				listacarros.add(newcar);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); //when car is not found
	}
	
	@DELETE
	@Path("/{id}/delete") 
	public Response deleteCar(@PathParam("id") long id) { //deletes an existing car in the list in the server
		SortedList<Car> listacarros = CarList.getInstance();
		for(int i=0; i<listacarros.size();i++) {
			if(id==listacarros.get(i).getCarId()) {
				listacarros.remove(listacarros.get(i));
				return Response.status(Response.Status.OK).build();
				
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); //car is not found
		
	}
	
	
	
	
	
	
	

	
}
