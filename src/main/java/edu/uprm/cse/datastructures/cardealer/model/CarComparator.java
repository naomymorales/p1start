package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	
	@Override 
	public int compare(Car c1, Car c2) //compares two cars 
	{
		if(!(c1 instanceof Car) && !(c2 instanceof Car))
		{
			throw new IllegalStateException("The object is not a car");
		}
		
		if(c1.getCarBrand().compareTo(c2.getCarBrand())==0) //compares the car brands
		{
			if(c1.getCarModel().compareTo(c2.getCarModel())==0) //compares the car models
			{
					return c1.getCarModelOption().compareTo(c2.getCarModelOption()); //compares the car model options
			}
			return c1.getCarModel().compareTo(c2.getCarModel());
		}
		return c1.getCarBrand().compareTo(c2.getCarBrand());
		
	}
	
}
	


