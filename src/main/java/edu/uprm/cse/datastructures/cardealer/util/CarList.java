
package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList {

    //creates a new CircularSortedDoublyLinkedList using Car Comparator
	public static CircularSortedDoublyLinkedList<Car> CarList = new CircularSortedDoublyLinkedList<Car>( new CarComparator());
	
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){ //creates an instance of the list
		return CarList;
		}
	
	//method to reset the list of cars
	public static void resetCars() {
		CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		}
}
