package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private static class Node<E>{ //node class
		private E element;
		private Node<E> prev;
		private Node<E> next;
		public Node(E e, Node<E> p, Node<E>  n) {
			this.element=e;
			this.prev=p;
			this.next=n;
			
		}
		public Node(E e) {
			this(e,null,null);
		}
		public E getElement() {
			return this.element;
			
		}
		public Node<E> getPrev(){
			return this.prev;
		}
		public Node<E> getNext(){
			return this.next;
		}
		public void setPrev(Node<E> p) {
			this.prev=p;
			
		}
		public void setNext(Node<E> n) {
			this.next=n;
			
		}
		public void setElement(E ele) {
			this.element=ele;
		}
		public E clear() {
			E elemento=this.element;
			element=null;
			return elemento;
		}
		
	}
	private Node<E> header;
	private int currentsize;
	private Comparator compa;
	
	public CircularSortedDoublyLinkedList() { // default constructor 
		this.header=new Node<>(null,header,header);
		this.currentsize=0;
	}
	
	public CircularSortedDoublyLinkedList(Comparator comp) { //constructor with comparator
		this.header=new Node<>(null,header,header);
		this.currentsize=0;
		this.compa=comp;
		
	}
	
	@Override
	public Iterator<E> iterator() { //iterates through list
		
		return new SortedCircularDoublyLinkedListIterator<E>();
	}
	//iterator class
	private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E>{

		private Node<E> current;
		
		public SortedCircularDoublyLinkedListIterator() {
			this.current=(Node<E>) header.getNext();
			
		}
		
		@Override
		public boolean hasNext() {
			return current!=header;
			
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result=current.getElement();
				current=current.getNext();
				return result;
			}
			throw new NoSuchElementException("No more elements");
		}
		
		
	}
	

	@Override
	public boolean add(E obj) { //adds a new element to the list
		if(this.isEmpty()) {
			Node<E> newnode=new Node<E>(obj,this.header,this.header);
			this.header.setNext(newnode);
			this.header.setPrev(newnode);
			this.currentsize++;
			return true;
		}
		else {
			Node<E> temp=header.getNext();
			while(temp.getElement()!=null) {
				if(this.compa.compare(obj, temp.getElement())<=0) { //sorts the elements by comparing their values with their respective Comparators
					Node<E> newnode= new Node(obj,temp.getPrev(),temp);
					temp.getPrev().setNext(newnode);
					temp.setPrev(newnode);
					this.currentsize++;
					return true;
				}
				temp=temp.getNext();
			}
			Node<E> newnode= new Node<E>(obj,this.header.getPrev(),this.header);
			this.header.getPrev().setNext(newnode);
			this.header.setPrev(newnode);
			this.currentsize++;
			return true;
			
		}
		
	}

	@Override
	public int size() { //return the size of the list
		
		return this.currentsize;
	}

	@Override
	public boolean remove(E obj) { //removes an element from the list
		if(this.isEmpty()) {
			return false;
		}
		if(!contains(obj)) {
			return false;
		}
		Node<E> curr=header.getNext();
		for(int i=0; i<this.currentsize;i++) {
			if(curr.getElement().equals(obj)) {
				curr.getPrev().setNext(curr.getNext());
				curr.getNext().setPrev(curr.getPrev());
				curr.clear();
				this.currentsize--;
				return true;
			}
			curr=curr.getNext();
		}
		
		return false;
	}

	@Override
	public boolean remove(int index) { //removes an element from the list using its index
		if(index<0|| index>=this.currentsize){
			throw new IndexOutOfBoundsException("Invalid index");
			
		}
		if(this.isEmpty()) {
			return false;
		}
		Node<E> temp=this.header.getNext();
		for(int i=0; i!=index;i++) {
			temp=temp.getNext();
		}
		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());
		temp.clear();
		this.currentsize--;
		return true;
	}

	@Override
	public int removeAll(E obj) { //removes all copies of the element in the list
		int count=0;
		while(this.remove(obj)){
			count++;
		}
		return count;
	}

	@Override
	public E first() { //returns first element in the list
		
		return (this.isEmpty() ? null: this.header.getNext().getElement());
	}

	@Override
	public E last() { //return last element of the list
		
		return (this.isEmpty() ? null : this.header.getPrev().getElement());
	}

	@Override
	public E get(int index) { //get an element from the list using its index
		if(this.isEmpty()) {
			return null;
		}
		if(index<0||index>=this.currentsize) {
			throw new IndexOutOfBoundsException("Invalid Index");
		}
		Node<E> curr=header.getNext();
		for(int i=0;i!=index;i++) {
			curr=curr.getNext();
		}
		return curr.getElement();
	}

	@Override
	public void clear() { //clears the list, deletes all the elements in the list
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
		
	}

	@Override
	public boolean contains(E e) { //checks if the element is in the list
		Node<E> curr=this.header.getNext();
		for(int i=0;i<this.size();i++) {
			if(curr.getElement().equals(e)) {
				return true;
			}
			curr=curr.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() { //checks if the list is empty
		
		return this.currentsize==0;
	}

	@Override
	public int firstIndex(E e) { //returns the first copy of the element in the list
		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp.getElement() != null; 
				temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}

	@Override
	public int lastIndex(E e) { //returns the last copy of the element in the list
		int i = currentsize-1;
		for (Node<E> temp = this.header.getPrev(); temp.getElement() != null; 
				temp = temp.getPrev(), --i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
		
	}
	
	
	

	
	

}
